package at.gleb.revolut

import androidx.test.rule.ActivityTestRule
import at.gleb.revolut.feature.currency.presentation.CurrencyActivity
import at.gleb.revolut.screen.CurrencyScreen
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Rule
import org.junit.Test

class CurrencyTest : TestCase() {

  @get:Rule
  val activityTestRule = ActivityTestRule(CurrencyActivity::class.java, true, true)

  @Test
  fun loadingState() {
    before {
    }.after {
    }.run {
      CurrencyScreen {
        flakySafely(timeoutMs = 10_000) {
          recycler.isDisplayed()
          recycler.firstChild<CurrencyScreen.Item> {
            isDisplayed()
          }
        }

        step("Try to click a lot times at the last item") {
          for (i in 0..5) {
            recycler.lastChild<CurrencyScreen.Item> {
              click()
            }
            recycler.firstChild<CurrencyScreen.Item> {
              isDisplayed()
              currencyValue.isFocused()
            }
          }
        }

        step("Pin item") {
          usdItem().click()
          recycler.firstChild<CurrencyScreen.Item> {
            hasTag("USD")
          }
        }
      }
    }
  }
}
