package at.gleb.revolut.screen

import android.view.View
import at.gleb.revolut.R
import at.gleb.revolut.feature.currency.presentation.CurrencyActivity
import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.progress.KProgressBar
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.text.KTextView
import org.hamcrest.Matcher

object CurrencyScreen : KScreen<CurrencyScreen>() {
  override val layoutId: Int? = R.layout.currency_main_layout
  override val viewClass: Class<*>? = CurrencyActivity::class.java

  val progressBar = KProgressBar { withId(R.id.progressBar) }

  val recycler = KRecyclerView(
      { withId(R.id.recyclerView) },
      { itemType(::Item) }
  )

  fun usdItem() = recycler.childWith<Item> {
    withTag("USD")
  }

  class Item(parent: Matcher<View>) : KRecyclerItem<Item>(parent) {
    val currencyValue = KEditText(parent) { withId(R.id.currencyValue) }
    val descrTextView = KTextView(parent) { withId(R.id.descrTextView) }
    val titleTextView = KTextView(parent) { withId(R.id.titleTextView) }
    val imageView = KImageView(parent) { withId(R.id.imageView) }
  }
}