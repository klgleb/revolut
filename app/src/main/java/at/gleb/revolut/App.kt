package at.gleb.revolut

import androidx.multidex.MultiDexApplication
import at.gleb.revolut.di.AppComponent
import at.gleb.revolut.di.DaggerAppComponent
import timber.log.Timber
import timber.log.Timber.DebugTree

class App : MultiDexApplication() {
  override fun onCreate() {
    super.onCreate()
    init()
  }

  private fun init() {
    dagger = DaggerAppComponent.builder().application(this).build()
    Timber.plant(DebugTree())
  }

  companion object {
    lateinit var dagger: AppComponent
  }
}