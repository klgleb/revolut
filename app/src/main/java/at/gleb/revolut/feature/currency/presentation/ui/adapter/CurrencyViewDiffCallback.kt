package at.gleb.revolut.feature.currency.presentation.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import at.gleb.revolut.feature.currency.domain.entities.CalculatedCurrency

class CurrencyViewDiffCallback : DiffUtil.ItemCallback<CalculatedCurrency>() {
  override fun areItemsTheSame(oldItem: CalculatedCurrency, newItem: CalculatedCurrency): Boolean {
    return oldItem.codeId == newItem.codeId
  }

  override fun areContentsTheSame(oldItem: CalculatedCurrency, newItem: CalculatedCurrency): Boolean {
    return oldItem == newItem
  }

  override fun getChangePayload(oldItem: CalculatedCurrency, newItem: CalculatedCurrency): Any? {
    return newItem.value
  }
}
