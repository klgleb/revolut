package at.gleb.revolut.feature.currency.presentation.ui

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import at.gleb.revolut.feature.currency.domain.entities.CurrencyValue
import io.reactivex.subjects.PublishSubject
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class CurrencyEditText : AppCompatEditText {
  constructor(context: Context) : super(context) {
    init()
  }

  constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
    init()
  }

  constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
    init()
  }

  val userEnterValueSubject: PublishSubject<CurrencyValue> = PublishSubject.create()

  var code: String? = null

  var value: Float? = null
    set(value) {
      if (isFocused) {
        return
      }

      if (value == 0f) {
        setText("")

      } else {

        val otherSymbols = DecimalFormatSymbols()
        otherSymbols.decimalSeparator = '.'
        val df = DecimalFormat("#.##", otherSymbols)
        df.roundingMode = RoundingMode.CEILING

        setText(df.format(value))
      }
    }

  private fun init() {
    addTextChangedListener(object : TextWatcher {
      override fun afterTextChanged(s: Editable?) {

      }

      override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
      }

      override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (isFocused) {
          emitFixValue()
        }
      }

    })
  }

  private fun emitFixValue() {
    code?.let { code ->
      val curAmount = text?.toString()?.toFloatOrNull() ?: 0f
      userEnterValueSubject.onNext(code to curAmount)
    }
  }
}