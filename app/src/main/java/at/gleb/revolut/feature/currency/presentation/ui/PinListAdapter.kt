package at.gleb.revolut.feature.currency.presentation.ui

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

abstract class PinListAdapter<T, VH : RecyclerView.ViewHolder>(diffCallback: DiffUtil.ItemCallback<T>)
  : ListAdapter<T, VH>(diffCallback) {

  fun pinItem(condition: (T) -> Boolean) {
    val newList = arrayListOf<T>()

    for (i in 0 until itemCount) {
      val cur = getItem(i)
      if (!condition(cur)) {
        newList.add(cur)
      } else {
        newList.add(0, cur)
      }
    }

    this.submitList(newList)
  }


}