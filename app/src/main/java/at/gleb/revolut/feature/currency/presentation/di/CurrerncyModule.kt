package at.gleb.revolut.feature.currency.presentation.di

import at.gleb.revolut.feature.currency.data.preferences.Prefs
import at.gleb.revolut.feature.currency.data.repository.CurrencyRepositoryImpl
import at.gleb.revolut.feature.currency.data.rest.RevolutApiService
import at.gleb.revolut.feature.currency.data.room.AppDatabase
import at.gleb.revolut.feature.currency.domain.boundary.CurrencyRepository
import at.gleb.revolut.feature.currency.domain.interactors.CurrencyInteractor
import at.gleb.revolut.utils.App
import dagger.Module
import dagger.Provides

@Module
class CurrerncyModule {
  @App
  @Provides
  fun provideCurrencyInteractor(currencyRepository: CurrencyRepository) = CurrencyInteractor(currencyRepository)

  @App
  @Provides
  fun provideCurrencyRepository(api: RevolutApiService, db: AppDatabase, prefs: Prefs): CurrencyRepository = CurrencyRepositoryImpl(api, db, prefs)
}