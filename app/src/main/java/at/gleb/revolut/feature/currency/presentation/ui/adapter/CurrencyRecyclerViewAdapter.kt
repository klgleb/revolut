package at.gleb.revolut.feature.currency.presentation.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import at.gleb.revolut.R
import at.gleb.revolut.feature.currency.domain.entities.CalculatedCurrency
import at.gleb.revolut.feature.currency.domain.entities.CurrencyValue
import at.gleb.revolut.feature.currency.presentation.ui.CurrencyEditText
import at.gleb.revolut.feature.currency.presentation.ui.PinListAdapter
import io.reactivex.subjects.PublishSubject

class CurrencyRecyclerViewAdapter(val callback: (String) -> Unit) : PinListAdapter<CalculatedCurrency, CurrencyViewHolder>(CurrencyViewDiffCallback()) {
  val userEnterValueSubject: PublishSubject<CurrencyValue> = PublishSubject.create()

  @SuppressLint("CheckResult")
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
    val inflater = LayoutInflater.from(parent.context)
    val itemView = inflater.inflate(R.layout.currency_item, parent, false)

    itemView.findViewById<CurrencyEditText>(R.id.currencyValue).userEnterValueSubject.subscribe({
      userEnterValueSubject.onNext(it)
    }, {
      //ignored
    })

    return CurrencyViewHolder(itemView)
  }

  override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
    val currency = getItem(position)
    holder.bindData(currency) {
      pinItem { item -> item.codeId == currency.codeId }
      callback(it)
    }
  }


}