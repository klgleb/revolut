package at.gleb.revolut.feature.currency.presentation.di

import androidx.lifecycle.ViewModel
import at.gleb.revolut.feature.currency.presentation.ui.viewmodel.CurrencyViewModel
import at.gleb.revolut.utils.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {
  @Binds
  @IntoMap
  @ViewModelKey(CurrencyViewModel::class)
  abstract fun currencyViewModel(currencyViewModel: CurrencyViewModel): ViewModel
}