package at.gleb.revolut.feature.currency.presentation

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import at.gleb.revolut.App
import at.gleb.revolut.R
import at.gleb.revolut.feature.currency.presentation.ui.adapter.CurrencyRecyclerViewAdapter
import at.gleb.revolut.feature.currency.presentation.ui.viewmodel.CurrencyScreenState
import at.gleb.revolut.feature.currency.presentation.ui.viewmodel.CurrencyViewModel
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject


class CurrencyActivity : AppCompatActivity() {
  @Inject
  lateinit var model: CurrencyViewModel

  private val recyclerView: RecyclerView by lazy {
    val rv = findViewById<RecyclerView>(R.id.recyclerView)
    rv.layoutManager = LinearLayoutManager(this)

    currencyAdapter = CurrencyRecyclerViewAdapter {
      model.onCurrencyClick(it)
    }.apply {
      registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
          if (toPosition == 0) {
            scrollToTop()
          }
        }
      })
    }

    disposable = currencyAdapter.userEnterValueSubject.subscribe({
      model.onEnterValue(it)
    }, {
      Timber.d(it)
    })

    rv.adapter = currencyAdapter

    //todo: do it better
    rv.setOnTouchListener { v, _ ->
      val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
      imm.hideSoftInputFromWindow(v.windowToken, 0)
      false
    }

    return@lazy rv
  }

  private val progressBar by lazy { findViewById<View>(R.id.progressBar) }

  private lateinit var currencyAdapter: CurrencyRecyclerViewAdapter

  private lateinit var disposable: Disposable

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.currency_main_layout)
    init()
  }

  private fun init() {
    App.dagger.inject(this)

    model.screenState.observe(this, Observer { state ->
      when (state) {
        is CurrencyScreenState.Loading -> manageProgressBar(true)
        is CurrencyScreenState.ShowCurrencies -> handleShowCurrencyState(state)
      }
    })
  }

  private fun manageProgressBar(visible: Boolean) {
    recyclerView.visibility = if (visible) View.GONE else View.VISIBLE
    progressBar.visibility = if (visible) View.VISIBLE else View.GONE
  }

  override fun onDestroy() {
    super.onDestroy()
    disposable.dispose()
  }

  private fun handleShowCurrencyState(state: CurrencyScreenState.ShowCurrencies) {
    manageProgressBar(false)
    currencyAdapter.submitList(state.currencies)
  }

  private fun scrollToTop() {
    recyclerView.postDelayed({
      recyclerView.layoutManager?.scrollToPosition(0)
    }, 10)
  }
}
