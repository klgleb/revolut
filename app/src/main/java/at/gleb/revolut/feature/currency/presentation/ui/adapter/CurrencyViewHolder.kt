package at.gleb.revolut.feature.currency.presentation.ui.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import at.gleb.revolut.App
import at.gleb.revolut.R
import at.gleb.revolut.common.glide.GlideApp
import at.gleb.revolut.feature.currency.domain.entities.CalculatedCurrency
import at.gleb.revolut.feature.currency.presentation.ui.CurrencyEditText
import com.bumptech.glide.request.RequestOptions


class CurrencyViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
  private val titleTextView: TextView = itemView.findViewById(R.id.titleTextView)
  private val descrTextView: TextView = itemView.findViewById(R.id.descrTextView)
  private val currencyValue: CurrencyEditText = itemView.findViewById(R.id.currencyValue)
  private val imageView: ImageView = itemView.findViewById(R.id.imageView)
  private val root: View = itemView.findViewById(R.id.container)

  fun bindData(currency: CalculatedCurrency, clickListener: (String) -> Unit) {
    root.tag = currency.codeId
    titleTextView.text = currency.symbol
    descrTextView.text = currency.description
    imageView.setImageResource(android.R.color.transparent)

    currencyValue.apply {
      code = currency.codeId
      value = currency.value
    }

    GlideApp.with(App.dagger.context)
        .load(currency.imageUrl)
        .apply(RequestOptions().circleCrop())
        .into(imageView)

    root.setOnClickListener {
      clickListener(currency.codeId)
      currencyValue.requestFocus()
    }
  }

}