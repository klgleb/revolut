package at.gleb.revolut.feature.currency.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import at.gleb.revolut.feature.currency.domain.interactors.CurrencyInteractor
import at.gleb.revolut.feature.currency.presentation.ui.viewmodel.CurrencyViewModel
import javax.inject.Inject

class NewsViewModelFactory @Inject constructor(
    private val interactor: CurrencyInteractor
) : ViewModelProvider.NewInstanceFactory() {
  override fun <T : ViewModel?> create(modelClass: Class<T>): T = CurrencyViewModel(interactor) as T
}