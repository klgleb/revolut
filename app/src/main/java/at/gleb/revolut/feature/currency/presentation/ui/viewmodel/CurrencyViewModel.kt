package at.gleb.revolut.feature.currency.presentation.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import at.gleb.revolut.feature.currency.domain.entities.CurrencyValue
import at.gleb.revolut.feature.currency.domain.interactors.CurrencyInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class CurrencyViewModel @Inject constructor(
    private val currencyInteractor: CurrencyInteractor
) : ViewModel() {

  private val disposable: CompositeDisposable = CompositeDisposable()
  private val _screenState = MutableLiveData<CurrencyScreenState>()
  val screenState = _screenState

  init {
    _screenState.value = CurrencyScreenState.Loading

    disposable += currencyInteractor.getCurrencies()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeBy(
            onNext = {
              _screenState.value = CurrencyScreenState.ShowCurrencies(it)
              Timber.d("showCurrencies")
            },
            onError = Timber::d
        )
  }

  fun onCurrencyClick(code: String) {
    disposable += currencyInteractor.pinCurrency(code).subscribeBy(
        onComplete = { Timber.d("Item $code was pinned successfully") },
        onError = Timber::d
    )
  }

  fun onEnterValue(currencyValue: CurrencyValue) {
    val (code, value) = currencyValue
    disposable += currencyInteractor.setEnteredCurrencyValue(code, value).subscribe({}, {})
  }

  override fun onCleared() {
    super.onCleared()
    disposable.dispose()
  }
}