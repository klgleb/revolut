package at.gleb.revolut.feature.currency.presentation.ui.viewmodel

import at.gleb.revolut.feature.currency.domain.entities.CalculatedCurrency


sealed class CurrencyScreenState {
  data class ShowCurrencies(val currencies: List<CalculatedCurrency>) : CurrencyScreenState()
  object Loading : CurrencyScreenState()
}