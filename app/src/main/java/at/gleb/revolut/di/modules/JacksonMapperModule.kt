package at.gleb.revolut.di.modules

import at.gleb.revolut.utils.App
import at.gleb.revolut.utils.JACKSON_MAPPER
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import dagger.Module
import dagger.Provides
import javax.inject.Named


@Module
class JacksonMapperModule {
  @App
  @Provides
  @Named(JACKSON_MAPPER)
  fun mapperKotlin(): ObjectMapper = jacksonObjectMapper().registerKotlinModule()
}