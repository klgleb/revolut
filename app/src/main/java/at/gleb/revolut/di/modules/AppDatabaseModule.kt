package at.gleb.revolut.di.modules

import android.app.Application
import androidx.room.Room
import at.gleb.revolut.feature.currency.data.room.AppDatabase
import at.gleb.revolut.utils.App
import dagger.Module
import dagger.Provides


@Module
class AppDatabaseModule {
  @App
  @Provides
  fun provideDb(context: Application) = Room.databaseBuilder(
      context,
      AppDatabase::class.java, "revolut"
  ).build()
}