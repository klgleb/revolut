package at.gleb.revolut.di.modules

import at.gleb.revolut.BuildConfig
import at.gleb.revolut.feature.currency.data.rest.RevolutApiService
import at.gleb.revolut.utils.App
import at.gleb.revolut.utils.RETROFIT_BUILDER
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
open class RevolutApiModule {

  @Provides
  @App
  fun provideApiService(@Named(RETROFIT_BUILDER) retrofitBuilder: Retrofit.Builder): RevolutApiService = retrofitBuilder
      .baseUrl(BuildConfig.SERVER_URL)
      .build()
      .create(RevolutApiService::class.java)

}