package at.gleb.revolut.di.modules

import at.gleb.revolut.BuildConfig
import at.gleb.revolut.utils.App
import at.gleb.revolut.utils.JACKSON_MAPPER
import at.gleb.revolut.utils.OK_HTTP_CLIENT
import at.gleb.revolut.utils.RETROFIT_BUILDER
import com.fasterxml.jackson.databind.ObjectMapper
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Named

@Module
class RetrofitModule{

  @App
  @Provides
  @Named(RETROFIT_BUILDER)
  fun retrofitBuilder(
      @Named(OK_HTTP_CLIENT) client: OkHttpClient,
      @Named(JACKSON_MAPPER) mapper: ObjectMapper
  ): Retrofit.Builder = Retrofit.Builder()
      .client(client)
      .addConverterFactory(JacksonConverterFactory.create(mapper))
      .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
      .baseUrl(BuildConfig.SERVER_URL)
}
