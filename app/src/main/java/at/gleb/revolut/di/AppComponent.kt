package at.gleb.revolut.di

import android.app.Application
import android.content.SharedPreferences
import at.gleb.revolut.di.modules.*
import at.gleb.revolut.feature.currency.data.di.modules.PreferencesModule
import at.gleb.revolut.feature.currency.data.preferences.Prefs
import at.gleb.revolut.feature.currency.data.room.AppDatabase
import at.gleb.revolut.feature.currency.domain.interactors.CurrencyInteractor
import at.gleb.revolut.feature.currency.presentation.CurrencyActivity
import at.gleb.revolut.feature.currency.presentation.di.CurrerncyModule
import at.gleb.revolut.utils.App
import dagger.BindsInstance
import dagger.Component

@App
@Component(
    modules = [
      JacksonMapperModule::class,
      OkHttpClientModule::class,
      PreferencesModule::class,
      RetrofitModule::class,
      RevolutApiModule::class,
      CurrerncyModule::class,
      AppDatabaseModule::class,
      CurrerncyModule::class
    ]
)
interface AppComponent {
  var prefs: Prefs
  var sharedPreferences: SharedPreferences
  var currencyInteractor: CurrencyInteractor
  var context: Application
  var db: AppDatabase

  fun inject(currencyActivity: CurrencyActivity)

  @Component.Builder
  interface Builder {

    fun build(): AppComponent

    @BindsInstance
    fun application(application: Application): Builder

  }
}