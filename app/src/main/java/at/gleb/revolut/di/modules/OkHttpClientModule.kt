package at.gleb.revolut.di.modules

import at.gleb.revolut.BuildConfig
import at.gleb.revolut.utils.App
import at.gleb.revolut.utils.OK_HTTP_CLIENT
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Named
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


@Module
class OkHttpClientModule {
  @App
  @Provides
  @Named("default")
  fun okHttpClientBuilder() = OkHttpClient.Builder()

  @App
  @Provides
  @Named(OK_HTTP_CLIENT)
  fun okHttpClient(@Named("default") defaultHttClientBuilder: OkHttpClient.Builder,
                   @Named("unsafe") unsafeOkHttClientBuilder: OkHttpClient.Builder
  ): OkHttpClient = if (BuildConfig.DEBUG) {
    unsafeOkHttClientBuilder
  } else {
    defaultHttClientBuilder
  }.build()


  @Provides
  @Named("unsafe")
  fun unsafeOkHttpClientBuilder(): OkHttpClient.Builder {
    val builder = OkHttpClient.Builder()

    try {
      // Create a trust manager that does not validate certificate chains
      val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
        override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {}

        override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {}

        override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
          return arrayOf()
        }
      })

      // Install the all-trusting trust manager
      val sslContext = SSLContext.getInstance("SSL")
      sslContext.init(null, trustAllCerts, java.security.SecureRandom())

      // Create an ssl socket factory with our all-trusting manager
      val sslSocketFactory = sslContext.socketFactory

      builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
      builder.hostnameVerifier(HostnameVerifier { _, _ -> true })
      return builder
    } catch (e: Exception) {
      throw RuntimeException(e)
    }

  }
}