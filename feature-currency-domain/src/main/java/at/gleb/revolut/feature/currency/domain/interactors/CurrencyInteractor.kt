package at.gleb.revolut.feature.currency.domain.interactors

import at.gleb.revolut.feature.currency.domain.boundary.CurrencyRepository
import at.gleb.revolut.feature.currency.domain.entities.CalculatedCurrency
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.zipWith
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CurrencyInteractor @Inject constructor(
    private val currencyRepository: CurrencyRepository
) {
  private val currenciesAfterValueChanges = currencyRepository.enteredValuesChanges
      .flatMapSingle {
        currencyRepository.currentCurrenciesFromDb.firstOrError()
      }

  private val currenciesFromServerEachSecond = Observable
      .interval(1, TimeUnit.SECONDS)
      .startWith(0)
      .flatMapSingle {
        currencyRepository.currencyFromServer
      }
      .retryWhen { it.delay(1, TimeUnit.SECONDS) }
      .doOnNext {
        currencyRepository.insertCurrenciesList(it).subscribe({}, {})
      }

  fun getCurrencies(): Observable<List<CalculatedCurrency>> = currencyRepository.currentCurrenciesFromDb.toObservable()
      .mergeWith(currenciesFromServerEachSecond.ignoreElements())
      .mergeWith(currenciesAfterValueChanges)
      .map { it.sortedByDescending { entityFromDb -> entityFromDb.weight } }
      .flatMapSingle {
        currencyRepository.enteredCurrencyValue.firstOrError().zipWith(Single.just(it))
      }
      .map { (enteredCurrency, list) ->
        val (codeOfChangedCurrency, enteredCurrencyValue) = enteredCurrency

        val changedCurrencyForOneEuro = list.find { currencyEntity ->
          currencyEntity.codeId == codeOfChangedCurrency
        }?.forOneEuro ?: 0f

        list.map { currencyEntity ->
          CalculatedCurrency(
              value = if (currencyEntity.codeId == codeOfChangedCurrency) {
                enteredCurrencyValue
              } else {
                (enteredCurrencyValue * currencyEntity.forOneEuro) / changedCurrencyForOneEuro
              },
              codeId = currencyEntity.codeId,
              description = currencyEntity.description,
              symbol = currencyEntity.symbol,
              imageUrl = currencyEntity.imageUrl
          )
        }
      }

  fun pinCurrency(code: String): Completable = currencyRepository.pinCurrency(code)

  fun setEnteredCurrencyValue(code: String, enteredValue: Float): Completable =
      currencyRepository.setEnteredCurrencyValue(code, enteredValue)
}