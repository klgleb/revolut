package at.gleb.revolut.feature.currency.domain.entities

typealias CurrencyValue = Pair<String, Float>