package at.gleb.revolut.feature.currency.domain.entities

data class CurrencyEntity(
    val codeId: String,
    var description: String,
    var symbol: String,
    var forOneEuro: Float,
    var imageUrl: String? = null,
    var weight: Int
)