package at.gleb.revolut.feature.currency.domain.boundary

import at.gleb.revolut.feature.currency.domain.entities.CurrencyEntity
import at.gleb.revolut.feature.currency.domain.entities.CurrencyValue
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject

interface CurrencyRepository {
  /**
   * Get current currency value that was entered by user including any changes
   */
  val enteredValuesChanges: Observable<CurrencyValue>

  /**
   * Get currency value that was entered by user from preferences
   */
  val enteredCurrencyValue: Observable<CurrencyValue>

  /**
   * Get currencies from local SQLite cache including any changes
   */
  val currentCurrenciesFromDb: Flowable<List<CurrencyEntity>>

  /**
   * Get currencies from server side
   */
  val currencyFromServer: Single<List<CurrencyEntity>>

  /**
   * Set maximum weigh to the currency by code
   */
  fun pinCurrency(code: String): Completable

  /**
   * Update currency value that was entered by user
   */
  fun setEnteredCurrencyValue(key: String, value: Float): Completable

  /**
   * Insert currencies to the SQLite local cache
   */
  fun insertCurrenciesList(list: List<CurrencyEntity>): Completable
}