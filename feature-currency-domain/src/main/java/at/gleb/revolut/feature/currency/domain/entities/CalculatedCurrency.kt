package at.gleb.revolut.feature.currency.domain.entities

data class CalculatedCurrency(
    val codeId: String,
    var description: String,
    var symbol: String,
    var value: Float,
    var imageUrl: String? = null
)