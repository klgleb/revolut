package at.gleb.revolut.feature.currency.domain.interactors

import at.gleb.revolut.feature.currency.domain.boundary.CurrencyRepository
import at.gleb.revolut.feature.currency.domain.entities.CalculatedCurrency
import at.gleb.revolut.feature.currency.domain.entities.CurrencyEntity
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Test

private const val CURRENCY_CHANGING_BY_USER_NAME = "currency that is changing by user"

class CurrencyInteractorTest {

  @Test
  fun getCurrencies() {
    assertCalculated(1f, 1f, 0.2f, 0.2f)
    assertCalculated(1f, 2f, 0.1f, 0.2f)
    assertCalculated(0.5f, 1f, 0.2f, 0.4f)
  }

  @Test
  fun pinItem() {
    val interactor = calculateInteractor(1f, 1f, 1f)
    val test = interactor.pinCurrency(CURRENCY_CHANGING_BY_USER_NAME).test()
    test.assertComplete().dispose()
  }

  /**
   * @param currencyForOneEuro Cost of currency which the user is changing for one euro
   * @param enteredValue Value of the currency that was entered by user
   */
  private fun assertCalculated(currencyForOneEuro: Float, enteredValue: Float, otherCurrencyInEuro: Float, expectedCalculatedValue: Float) {

    val interactor = calculateInteractor(currencyForOneEuro, enteredValue, otherCurrencyInEuro)
    val currencies = interactor.getCurrencies().test()

    val testCurrency = testCurrency(otherCurrencyInEuro)
    val value1 = CalculatedCurrency(testCurrency.codeId, testCurrency.description, testCurrency.symbol, expectedCalculatedValue, null)
    val value2 = CalculatedCurrency(CURRENCY_CHANGING_BY_USER_NAME, "", "", enteredValue, null)

    currencies
        .assertValueAt(currencies.valueCount() - 1, listOf(value1, value2))
        .dispose()
  }

  private fun calculateInteractor(currencyForOneEuro: Float, valueEnteredByUser: Float, otherCurrencyInEuro: Float): CurrencyInteractor {
    val repository = mock<CurrencyRepository>()

    whenever(repository.currentCurrenciesFromDb).thenReturn(Flowable.just(
        listOf(
            testCurrency(otherCurrencyInEuro),
            CurrencyEntity(CURRENCY_CHANGING_BY_USER_NAME, "", "", currencyForOneEuro, null, 1)
        )
    ))

    whenever(repository.enteredCurrencyValue).thenReturn(Observable.just(CURRENCY_CHANGING_BY_USER_NAME to valueEnteredByUser))

    whenever(repository.enteredValuesChanges).thenReturn(Observable.never())
    whenever(repository.currencyFromServer).thenReturn(Single.never())
    whenever(repository.pinCurrency(any())).thenReturn(Completable.complete())
    whenever(repository.setEnteredCurrencyValue(any(), any())).thenReturn(Completable.complete())
    whenever(repository.insertCurrenciesList(any())).thenReturn(Completable.complete())

    return CurrencyInteractor(repository)
  }

  private fun testCurrency(forOneEuro: Float) = CurrencyEntity("test currency", "test currency", "MOCK", forOneEuro, null, 2)
}