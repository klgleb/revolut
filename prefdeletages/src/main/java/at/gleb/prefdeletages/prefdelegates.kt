package at.gleb.prefdeletages

import android.content.SharedPreferences
import android.os.Looper
import kotlin.reflect.KProperty

/**
 * Delegates for preferences
 *
 * Created by gleb, info@gleb.at,  on 29/01/2018.
 */

private lateinit var prefs: SharedPreferences

object PrefDelegates {
  fun init(sharedPreferences: SharedPreferences) {
    prefs = sharedPreferences
  }
}

class StringPref(private val defValue: String, private val prefKey: String? = null) {
  operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
    checkNotMainThread()
    return prefs.getString(prefKey ?: property.name, defValue)!!
  }

  operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
    checkNotMainThread()
    prefs.edit()
        .putString(prefKey ?: property.name, value)
        .apply()
  }
}


class IntPref(private val defValue: Int, private val prefKey: String? = null) {
  operator fun getValue(thisRef: Any?, property: KProperty<*>): Int {
    checkNotMainThread()
    return prefs.getInt(prefKey ?: property.name, defValue)
  }

  operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) {
    checkNotMainThread()
    prefs.edit()
        .putInt(prefKey ?: property.name, value)
        .apply()
  }
}

class BooleanPref(private val defValue: Boolean, private val prefKey: String? = null) {
  operator fun getValue(thisRef: Any?, property: KProperty<*>): Boolean {
    checkNotMainThread()
    return prefs.getBoolean(prefKey ?: property.name, defValue)
  }

  operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean) {
    checkNotMainThread()
    prefs.edit()
        .putBoolean(prefKey ?: property.name, value)
        .apply()
  }
}

class LongPref(private val defValue: Long, private val prefKey: String? = null) {
  operator fun getValue(thisRef: Any?, property: KProperty<*>): Long {
    checkNotMainThread()
    return prefs.getLong(prefKey ?: property.name, defValue)
  }

  operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Long) {
    checkNotMainThread()
    prefs.edit()
        .putLong(prefKey ?: property.name, value)
        .apply()
  }
}

class DoublePref(private val defValue: Double, private val prefKey: String? = null) {
  operator fun getValue(thisRef: Any?, property: KProperty<*>): Double {
    checkNotMainThread()
    val defValue = java.lang.Double.doubleToRawLongBits(defValue)
    val longValue = prefs.getLong(prefKey ?: property.name, defValue)
    return java.lang.Double.longBitsToDouble(longValue)
  }

  operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Double) {
    checkNotMainThread()
    val longValue = java.lang.Double.doubleToRawLongBits(value)
    prefs.edit()
        .putLong(prefKey ?: property.name, longValue)
        .apply()
  }
}


class FloatPref(private val defValue: Float, private val prefKey: String? = null) {
  operator fun getValue(thisRef: Any?, property: KProperty<*>): Float {
    checkNotMainThread()
    return prefs.getFloat(prefKey ?: property.name, defValue)
  }

  operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Float) {
    checkNotMainThread()
    prefs.edit()
        .putFloat(prefKey ?: property.name, value)
        .apply()
  }
}

class StringSetPref(private val defValue: MutableSet<String>, private val prefKey: String? = null) {
  operator fun getValue(thisRef: Any?, property: KProperty<*>): MutableSet<String> {
    checkNotMainThread()
    return prefs.getStringSet(prefKey ?: property.name, defValue)!!
  }

  operator fun setValue(thisRef: Any?, property: KProperty<*>, value: MutableSet<String>) {
    checkNotMainThread()
    prefs.edit()
        .putStringSet(prefKey ?: property.name, value)
        .apply()
  }
}

private fun checkNotMainThread() {
  if (Looper.myLooper() == Looper.getMainLooper()) {
    throw IllegalStateException("Cannot access preferences on the main thread since it may potentially lock the UI for a long periods of time")
  }
}