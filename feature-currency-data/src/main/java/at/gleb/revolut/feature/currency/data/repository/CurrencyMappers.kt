package at.gleb.revolut.feature.currency.data.repository

import at.gleb.revolut.feature.currency.data.room.entity.CurrencyRoomEntity
import at.gleb.revolut.feature.currency.domain.entities.CurrencyEntity
import at.gleb.revolut.feature.currency.data.CurrencyResponse
import at.gleb.revolut.feature.currency.data.baseCurrencyEntity
import at.gleb.revolut.utils.FLAGS_URL
import java.util.*

fun CurrencyResponse.toCurrencyEntities(): List<CurrencyEntity> = rates.mapNotNull {
  dataToCurrencyEntity(it.key, it.value, 0)
}.toMutableList().apply {
  add(baseCurrencyEntity.toCurrencyEntity())
}

fun CurrencyRoomEntity.toCurrencyEntity() = dataToCurrencyEntity(key, value, weight)

fun dataToCurrencyEntity(key: String, valueForOneEuro: Float, weight: Int): CurrencyEntity {
  val c: Currency? = Currency.getInstance(key)
  return CurrencyEntity(
      key,
      c?.displayName ?: "",
      c?.symbol ?: key,
      valueForOneEuro,
      "$FLAGS_URL${key.toLowerCase()}.png",
      weight
  )
}