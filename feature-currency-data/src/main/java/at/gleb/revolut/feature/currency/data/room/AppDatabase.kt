package at.gleb.revolut.feature.currency.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import at.gleb.revolut.feature.currency.data.room.entity.CurrencyRoomEntity

@Database(entities = [CurrencyRoomEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
  abstract fun currencyDao(): CurrencyDao
}