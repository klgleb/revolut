package at.gleb.revolut.feature.currency.data.preferences

import at.gleb.prefdeletages.FloatPref
import at.gleb.prefdeletages.StringPref
import at.gleb.revolut.feature.currency.data.baseCurrencyEntity

class Prefs {
  var enteredCurrencyCode by StringPref(baseCurrencyEntity.key)
  var enteredCurrencyAmount by FloatPref(1F)
}