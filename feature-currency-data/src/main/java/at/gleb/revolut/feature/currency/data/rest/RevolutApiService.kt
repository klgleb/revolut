package at.gleb.revolut.feature.currency.data.rest

import at.gleb.revolut.feature.currency.data.CurrencyResponse
import at.gleb.revolut.feature.currency.data.baseCurrencyEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutApiService {
  @GET("latest")
  fun currencies(@Query("base") base: String = baseCurrencyEntity.key): Single<CurrencyResponse>
}