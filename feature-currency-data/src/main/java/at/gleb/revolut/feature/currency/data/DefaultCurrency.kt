package at.gleb.revolut.feature.currency.data

import at.gleb.revolut.feature.currency.data.room.entity.CurrencyRoomEntity

val baseCurrencyEntity = CurrencyRoomEntity("EUR", 1F, 1)