package at.gleb.revolut.feature.currency.data.repository

import at.gleb.revolut.feature.currency.data.preferences.Prefs
import at.gleb.revolut.feature.currency.data.rest.RevolutApiService
import at.gleb.revolut.feature.currency.data.room.AppDatabase
import at.gleb.revolut.feature.currency.data.room.entity.CurrencyRoomEntity
import at.gleb.revolut.feature.currency.domain.boundary.CurrencyRepository
import at.gleb.revolut.feature.currency.domain.entities.CurrencyEntity
import at.gleb.revolut.feature.currency.domain.entities.CurrencyValue
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class CurrencyRepositoryImpl @Inject constructor(
    currencyApiService: RevolutApiService,
    private val appDatabase: AppDatabase,
    private val prefs: Prefs
) : CurrencyRepository {

  override val enteredValuesChanges = BehaviorSubject.create<CurrencyValue>()


  override val enteredCurrencyValue: Observable<CurrencyValue> = Observable
      .fromCallable {
        prefs.enteredCurrencyCode to prefs.enteredCurrencyAmount
      }
      .mergeWith(enteredValuesChanges)
      .subscribeOn(Schedulers.io())

  override val currentCurrenciesFromDb: Flowable<List<CurrencyEntity>> = appDatabase.currencyDao().getAll()
      .map { roomEntities ->
        roomEntities.map { it.toCurrencyEntity() }
      }
      .subscribeOn(Schedulers.io())

  override val currencyFromServer: Single<List<CurrencyEntity>> = currencyApiService.currencies()
      .map {
        it.toCurrencyEntities()
      }
      .subscribeOn(Schedulers.io())

  override fun pinCurrency(code: String) = appDatabase.currencyDao().pinItem(code)
      .subscribeOn(Schedulers.io())

  override fun setEnteredCurrencyValue(key: String, value: Float) = Completable
      .fromCallable {
        enteredValuesChanges.onNext(key to value)
      }
      .doOnComplete {
        prefs.enteredCurrencyCode = key
        prefs.enteredCurrencyAmount = value
      }
      .subscribeOn(Schedulers.io())

  override fun insertCurrenciesList(list: List<CurrencyEntity>): Completable = Completable
      .fromCallable {
        val roomEntities = list.map {
          CurrencyRoomEntity(it.codeId, it.forOneEuro)
        }
        appDatabase.currencyDao().insertCurrenciesList(roomEntities)
      }
      .subscribeOn(Schedulers.io())
}