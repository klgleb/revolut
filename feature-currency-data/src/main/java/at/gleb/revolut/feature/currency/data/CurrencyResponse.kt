package at.gleb.revolut.feature.currency.data

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*

data class CurrencyResponse(
    val base: String,

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    val date: Date,

    val rates: Map<String, Float>
)