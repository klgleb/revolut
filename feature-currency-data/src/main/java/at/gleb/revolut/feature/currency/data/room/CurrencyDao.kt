package at.gleb.revolut.feature.currency.data.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import at.gleb.revolut.feature.currency.data.room.entity.CurrencyRoomEntity
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface CurrencyDao {
  @Query("SELECT * FROM currencies")
  fun getAll(): Flowable<List<CurrencyRoomEntity>>

  @Query("SELECT * FROM currencies")
  fun getAllList(): List<CurrencyRoomEntity>

  @Insert
  fun insertAll(entities: List<CurrencyRoomEntity>)

  @Query("DELETE FROM currencies")
  fun clear()

  /**
   * Insert or update all currency values (not 'weight' field),
   * and delete currencies that aren't in the incoming [entities]
   */
  @Transaction
  fun insertCurrenciesList(entities: List<CurrencyRoomEntity>) {
    val oldList = getAllList()

    if (oldList.isNotEmpty()) {
      entities.map { cur ->
        oldList.find { it.key == cur.key }?.let {
          cur.weight = it.weight
        }
      }
    }

    clear()
    insertAll(entities)
  }

  //note: don't care about user doing Integer.MAX_VALUE clicks, it's 2451 clicks per hour during 100 years
  @Query("UPDATE currencies SET weight=((SELECT max(weight) FROM  currencies) + 1) WHERE `key` = :code")
  fun pinItem(code: String): Completable
}