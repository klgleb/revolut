package at.gleb.revolut.feature.currency.data.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currencies")
data class CurrencyRoomEntity(
    @PrimaryKey
    var key: String,

    var value: Float,

    var weight: Int = 0
)