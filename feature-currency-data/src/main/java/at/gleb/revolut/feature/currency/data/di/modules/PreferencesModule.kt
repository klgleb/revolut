package at.gleb.revolut.feature.currency.data.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import at.gleb.prefdeletages.PrefDelegates
import at.gleb.revolut.feature.currency.data.preferences.Prefs
import at.gleb.revolut.utils.App
import dagger.Module
import dagger.Provides

@Module
class PreferencesModule {

  @App
  @Provides
  fun preferences(preferences: SharedPreferences): Prefs {
    PrefDelegates.init(preferences)
    return Prefs()
  }

  @App
  @Provides
  fun provideSharedPreferences(context: Application): SharedPreferences =
      context.getSharedPreferences("at.gleb.revolut", Context.MODE_PRIVATE)
}