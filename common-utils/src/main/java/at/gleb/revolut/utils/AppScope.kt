package at.gleb.revolut.utils

import javax.inject.Scope

@Scope
@Retention
annotation class App